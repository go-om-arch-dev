package main

import "fmt"

func main() {
	ch := make(chan int, 2)
	fmt.Println("make后，len(ch) = ", len(ch))

	ch <- 1
	fmt.Println("写入1个值后，len(ch) = ", len(ch))
	ch <- 2
	fmt.Println("写入2个值后，len(ch) = ", len(ch))
	//ch <- 3 //死锁

	fmt.Println(<-ch)
	fmt.Println("读取1个值后，len(ch) = ", len(ch))
	fmt.Println(<-ch)
	fmt.Println("读取2个值后，len(ch) = ", len(ch))

	//close(ch)  // 关闭后不可写
	ch <- 3
	close(ch) //关闭后可读
	fmt.Println(<-ch)
}
