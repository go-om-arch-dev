package main

import (
	"fmt"
	"time"
)

func main() {
	ch1 := make(chan int, 0)
	ch2 := make(chan int, 0)

	go func() {
		ch1 <- 1
	}()

	go func() {
		ch2 <- 2
	}()

	select {
	case i := <-ch1:
		fmt.Println("从ch1读取到：", i)
	case i := <-ch2:
		fmt.Println("从ch2读取到：", i)
	case t := <-time.After(time.Second):
		fmt.Println("超时：", t)
	}
}
