package main

import (
	"fmt"
	"runtime"
	"sync"
)

func printA2Z(name string, wg *sync.WaitGroup) {
	defer wg.Done()
	for i := 'A'; i <= 'Z'; i++ {
		fmt.Printf("%s, %c. ", name, i)
		runtime.Gosched()
	}
}
func main() {
	var wg sync.WaitGroup
	wg.Add(3)
	go printA2Z("甲", &wg)
	go printA2Z("乙", &wg)
	go printA2Z("丙", &wg)
	wg.Wait()
}
