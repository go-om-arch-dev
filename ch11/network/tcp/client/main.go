package main

import (
	"log"
	"net"
)

func main() {
	conn, err := net.Dial("tcp", "127.0.0.1:9527")
	if err != nil {
		log.Fatal("Dial to server failed:", err)
	}
	defer conn.Close()

	buf := make([]byte, 1024)
	n, err := conn.Read(buf)
	if err != nil {
		log.Fatal("Read data from server failed:", err)
	}
	log.Printf("Read %d data from server: %s\n", n, buf[:n])
}
