package main

import (
	"fmt"
	"runtime"
	"time"
)

func printA2Z(name string) {
	for i := 'A'; i <= 'Z'; i++ {
		fmt.Printf("%s, %c\n", name, i)
		runtime.Gosched()
	}
}

func main() {
	go printA2Z("甲")
	go printA2Z("乙")
	printA2Z("丙")
	time.Sleep(time.Second)
}
