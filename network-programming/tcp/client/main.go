package main

import (
	"bufio"
	"log"
	"net"
	"os"

	"repo.or.cz/go-om-arch-dev/network-programming/tcp"
)

func main() {
	conn, err := net.Dial("tcp", tcp.ServerAddr)
	if err != nil {
		log.Fatal("Dial to server failed:", err)
	}
	defer conn.Close()

	reader := bufio.NewReader(conn)
	writer := bufio.NewWriter(conn)
	scaner := bufio.NewScanner(os.Stdin)
	for {
		scaner.Scan()
		msg := scaner.Text()
		if msg == "quit" {
			log.Println("再见")
			break
		}
		if _, err := writer.WriteString(msg + "\n"); err != nil {
			log.Println("Write data to server failed:", err)
			continue
		}
		writer.Flush()

		recMsg, err := reader.ReadString('\n')
		if err != nil {
			log.Println("Read data from server failed:", err)
			continue
		}
		log.Print(recMsg)
	}
}
