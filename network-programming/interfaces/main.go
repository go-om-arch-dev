package main

import (
	"fmt"
	"net"
)

func main() {
	is, _ := net.Interfaces()
	for _, i := range is {
		fmt.Printf("%#v, %s\n", i, i.HardwareAddr)
		addrs, _ := i.Addrs()
		fmt.Printf("ips of %s: %v\n", i.Name, addrs)
		fmt.Println()
	}
}
