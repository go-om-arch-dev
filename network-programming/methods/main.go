package main

import (
	"fmt"
	"net"
)

func main() {
	fmt.Println(net.JoinHostPort("127.0.0.1", "9527"))
	fmt.Println(net.JoinHostPort("::1", "9527"))

	fmt.Println(net.SplitHostPort("127.0.0.1:9527"))
	fmt.Println(net.SplitHostPort("[::1]:9527"))

	fmt.Println(net.LookupAddr("140.82.113.4"))
	fmt.Println(net.LookupCNAME("github.com"))
	fmt.Println(net.LookupHost("github.com"))
	fmt.Println(net.LookupIP("github.com"))

	// IP 结构体
	ip := net.ParseIP("140.82.113.4")
	fmt.Printf("%#v\n", ip)

	// IPNet：IP范围, CIDR 掩码
	ip, ipNet, err := net.ParseCIDR("192.168.1.1/24")
	fmt.Printf("%v, %#v, %s,  %v\n", ip, ipNet, ipNet, err)
	fmt.Println(ipNet.Contains(ip))
	fmt.Println(ipNet.Contains(net.ParseIP("140.82.113.4")))
	fmt.Println(ipNet.Contains(net.ParseIP("192.168.2.1")))
}
