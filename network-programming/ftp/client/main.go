package main

import (
	"bufio"
	"encoding/base64"
	"log"
	"net"
	"os"
	"strings"
)

func main() {
	conn, err := net.Dial("tcp", "127.0.0.1:9981")
	if err != nil {
		log.Fatal("Dial failed:", err)
	}
	defer conn.Close()

	r := bufio.NewReader(conn)
	w := bufio.NewWriter(conn)
	s := bufio.NewScanner(os.Stdin)
	for {
		s.Scan()
		derive := strings.TrimSpace(s.Text())

		if derive == "quit" {
			log.Println("Bye!")
			break
		}

		if _, err := w.WriteString(derive + "\n"); err != nil {
			log.Println("Send dervie to server failed:", err)
			continue
		}
		w.Flush()

		data, err := r.ReadString('\n')
		if err != nil {
			log.Println("Recvie data from server failed:", err)
			continue
		}

		decodedData, err := base64.StdEncoding.DecodeString(data)
		if err != nil {
			log.Println("Decode data from server failed:", err)
			continue
		}
		log.Print(string(decodedData))
	}
}
