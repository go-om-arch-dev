package main

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"io/ioutil"
	"os"
)

func main() {
	s := md5Str([]byte("风从东方来"))
	fmt.Println(s)

	if buf, err := ioutil.ReadFile("./main.go"); err != nil {
		fmt.Println(err)
		return
	} else {
		fmt.Println(md5Str(buf))
	}

	if file, err := os.Open("./main.go"); err == nil {
		defer file.Close()
		var buf = make([]byte, 20)
		m := md5.New()
		for {
			n, err := file.Read(buf)
			if err != nil {
				break
			}
			if n == 0 {
				break
			}
			m.Write(buf[:n])
		}
		fmt.Printf("%x\n", m.Sum(nil))
	}

}

func md5Str(b []byte) string {
	//return fmt.Sprintf("%x", md5.Sum(b))
	s := md5.Sum(b)
	return hex.EncodeToString(s[:])
}
